var $activeElement = null;
// Set active menu item for home
jQuery(document).ready(function ($) {
    setTimeout(() => {
        $(".link-home").addClass("active");
    }, 100);

    $("#contacto").on("show.bs.modal", function(e){
        $activeElement = $(document.activeElement);
        $activeElement.addClass('btn-primary').removeClass('btn-outline-success').prop('disabled', true);
        console.log("Evento de modal ejecutandose");
    });

    $("#contacto").on("shown.bs.modal", function(e){
        console.log("Evento de modal ejecutado");
    });

    $("#contacto").on("hide.bs.modal", function(e){
        $activeElement.removeClass('btn-primary').addClass('btn-outline-success').prop('disabled', false);
        console.log("El modal se oculta");
    });

    $("#contacto").on("hidden.bs.modal", function(e){
        console.log("El moda se ocultó");
    });
});

jQuery(function($){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $(".carousel").carousel({
        interval: 2000
    });
});